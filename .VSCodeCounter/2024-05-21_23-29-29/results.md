# Summary

Date : 2024-05-21 23:29:29

Directory /Users/gvilloslado/Projects/django-one-shot

Total : 45 files,  2258 codes, 22 comments, 379 blanks, all 2659 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Python | 32 | 1,729 | 22 | 304 | 2,055 |
| HTML | 8 | 325 | 0 | 16 | 341 |
| CSS | 1 | 109 | 0 | 20 | 129 |
| Markdown | 2 | 62 | 0 | 36 | 98 |
| pip requirements | 1 | 27 | 0 | 1 | 28 |
| toml | 1 | 6 | 0 | 2 | 8 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 45 | 2,258 | 22 | 379 | 2,659 |
| . (Files) | 5 | 112 | 1 | 44 | 157 |
| brain_two | 5 | 121 | 16 | 57 | 194 |
| tests | 15 | 1,352 | 0 | 182 | 1,534 |
| todos | 20 | 673 | 5 | 96 | 774 |
| todos (Files) | 8 | 181 | 3 | 46 | 230 |
| todos/migrations | 3 | 58 | 2 | 14 | 74 |
| todos/static | 1 | 109 | 0 | 20 | 129 |
| todos/static/css | 1 | 109 | 0 | 20 | 129 |
| todos/templates | 8 | 325 | 0 | 16 | 341 |
| todos/templates (Files) | 1 | 39 | 0 | 2 | 41 |
| todos/templates/todos | 7 | 286 | 0 | 14 | 300 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)