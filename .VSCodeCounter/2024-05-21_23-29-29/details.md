# Details

Date : 2024-05-21 23:29:29

Directory /Users/gvilloslado/Projects/django-one-shot

Total : 45 files,  2258 codes, 22 comments, 379 blanks, all 2659 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [README.md](/README.md) | Markdown | 46 | 0 | 22 | 68 |
| [brain_two/__init__.py](/brain_two/__init__.py) | Python | 0 | 0 | 1 | 1 |
| [brain_two/asgi.py](/brain_two/asgi.py) | Python | 10 | 0 | 7 | 17 |
| [brain_two/settings.py](/brain_two/settings.py) | Python | 76 | 16 | 35 | 127 |
| [brain_two/urls.py](/brain_two/urls.py) | Python | 25 | 0 | 7 | 32 |
| [brain_two/wsgi.py](/brain_two/wsgi.py) | Python | 10 | 0 | 7 | 17 |
| [manage.py](/manage.py) | Python | 17 | 1 | 5 | 23 |
| [planning.md](/planning.md) | Markdown | 16 | 0 | 14 | 30 |
| [pyproject.toml](/pyproject.toml) | toml | 6 | 0 | 2 | 8 |
| [requirements.txt](/requirements.txt) | pip requirements | 27 | 0 | 1 | 28 |
| [tests/__init__.py](/tests/__init__.py) | Python | 1 | 0 | 1 | 2 |
| [tests/test_feature_01.py](/tests/test_feature_01.py) | Python | 27 | 0 | 7 | 34 |
| [tests/test_feature_02.py](/tests/test_feature_02.py) | Python | 18 | 0 | 6 | 24 |
| [tests/test_feature_03.py](/tests/test_feature_03.py) | Python | 93 | 0 | 15 | 108 |
| [tests/test_feature_04.py](/tests/test_feature_04.py) | Python | 14 | 0 | 4 | 18 |
| [tests/test_feature_05.py](/tests/test_feature_05.py) | Python | 181 | 0 | 27 | 208 |
| [tests/test_feature_06.py](/tests/test_feature_06.py) | Python | 14 | 0 | 4 | 18 |
| [tests/test_feature_07.py](/tests/test_feature_07.py) | Python | 122 | 0 | 11 | 133 |
| [tests/test_feature_08.py](/tests/test_feature_08.py) | Python | 104 | 0 | 11 | 115 |
| [tests/test_feature_09.py](/tests/test_feature_09.py) | Python | 115 | 0 | 13 | 128 |
| [tests/test_feature_10.py](/tests/test_feature_10.py) | Python | 120 | 0 | 13 | 133 |
| [tests/test_feature_11.py](/tests/test_feature_11.py) | Python | 102 | 0 | 12 | 114 |
| [tests/test_feature_12.py](/tests/test_feature_12.py) | Python | 155 | 0 | 16 | 171 |
| [tests/test_feature_13.py](/tests/test_feature_13.py) | Python | 161 | 0 | 16 | 177 |
| [tests/utils.py](/tests/utils.py) | Python | 125 | 0 | 26 | 151 |
| [todos/__init__.py](/todos/__init__.py) | Python | 0 | 0 | 1 | 1 |
| [todos/admin.py](/todos/admin.py) | Python | 15 | 1 | 5 | 21 |
| [todos/apps.py](/todos/apps.py) | Python | 4 | 0 | 3 | 7 |
| [todos/forms.py](/todos/forms.py) | Python | 41 | 0 | 11 | 52 |
| [todos/migrations/0001_initial.py](/todos/migrations/0001_initial.py) | Python | 22 | 1 | 7 | 30 |
| [todos/migrations/0002_todoitem.py](/todos/migrations/0002_todoitem.py) | Python | 36 | 1 | 6 | 43 |
| [todos/migrations/__init__.py](/todos/migrations/__init__.py) | Python | 0 | 0 | 1 | 1 |
| [todos/models.py](/todos/models.py) | Python | 20 | 1 | 7 | 28 |
| [todos/static/css/todos.css](/todos/static/css/todos.css) | CSS | 109 | 0 | 20 | 129 |
| [todos/templates/base.html](/todos/templates/base.html) | HTML | 39 | 0 | 2 | 41 |
| [todos/templates/todos/create.html](/todos/templates/todos/create.html) | HTML | 34 | 0 | 2 | 36 |
| [todos/templates/todos/create_item.html](/todos/templates/todos/create_item.html) | HTML | 34 | 0 | 2 | 36 |
| [todos/templates/todos/delete.html](/todos/templates/todos/delete.html) | HTML | 36 | 0 | 2 | 38 |
| [todos/templates/todos/detail.html](/todos/templates/todos/detail.html) | HTML | 58 | 0 | 2 | 60 |
| [todos/templates/todos/edit.html](/todos/templates/todos/edit.html) | HTML | 37 | 0 | 2 | 39 |
| [todos/templates/todos/edit_item.html](/todos/templates/todos/edit_item.html) | HTML | 37 | 0 | 2 | 39 |
| [todos/templates/todos/list.html](/todos/templates/todos/list.html) | HTML | 50 | 0 | 2 | 52 |
| [todos/tests.py](/todos/tests.py) | Python | 1 | 1 | 2 | 4 |
| [todos/urls.py](/todos/urls.py) | Python | 19 | 0 | 2 | 21 |
| [todos/views.py](/todos/views.py) | Python | 81 | 0 | 15 | 96 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)