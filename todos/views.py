from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from django.http import HttpRequest, HttpResponse
from todos.forms import (
    TodoListForm,
    TodoItemForm,
    EditTodoListForm,
    EditTodoItemForm,
)


def todo_list_list(request: HttpRequest) -> HttpResponse:
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def show_list(request: HttpRequest, id) -> HttpResponse:
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist": todolist,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save(False)
            todolist.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm()
        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save(False)
            todoitem.save()
            return redirect("todo_list_detail", id=todoitem.list.id)
    else:
        form = TodoItemForm()
        context = {
            "form": form,
        }
        return render(request, "todos/create_item.html", context)


def edit_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        edit_form = EditTodoListForm(request.POST, instance=todolist)
        if edit_form.is_valid():
            edit_form.save()
            return redirect("todo_list_detail", id=id)
    else:
        edit_form = EditTodoListForm(instance=todolist)
        context = {
            "edit_form": edit_form,
            "todolist": todolist,
        }
        return render(request, "todos/edit.html", context)


def edit_item(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        edit_item_form = EditTodoItemForm(request.POST, instance=todoitem)
        if edit_item_form.is_valid():
            updated_item = edit_item_form.save()
            return redirect("todo_list_detail", id=updated_item.list.id)
    else:
        edit_item_form = EditTodoItemForm(instance=todoitem)
        context = {
            "edit_item_form": edit_item_form,
            "todoitem": todoitem,
        }
        return render(request, "todos/edit_item.html", context)


def delete_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
